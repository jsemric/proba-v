#!/usr/bin/env python

import numpy as np
import argparse
import os
import matplotlib.pyplot as plt
import warnings
warnings.simplefilter("ignore", UserWarning)
from glob import glob
from PIL import Image
from tensorflow.contrib import predictor
from skimage.transform import resize

def np_psnr(y_true, y_pred):
    return -10. * np.log(np.mean(np.square(y_pred - y_true)))

def plot_imgs(pred, interp, hr):
    fig, ax = plt.subplots(2,1)
    ax[0].imshow(np.hstack([pred, interp, hr]), cmap='gray')
    ax[1].imshow(np.hstack([np.square(hr - pred), np.square(hr - interp),
        np.square(interp - pred)]), cmap='gray')

    ax[0].axis('off')
    ax[0].set_title('Interpolation: [{:.4f}]   Neural Network: [{:.4f}]'\
        .format(np_psnr(pred,hr),np_psnr(interp,hr)))

    ax[1].axis('off')
    ax[1].set_title('Residuals hr-lr/hr-pred/lr-pred')
    return fig

def load_imageset(path):
    C = 65535.
    lr_paths = glob(os.path.join(path,'LR*.png'))
    lrs = np.array([np.array(Image.open(i)) / C for i in lr_paths])
    hr = np.array(Image.open(os.path.join(path,'HR.png'))) / C
    return lrs, hr

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', type=str, default='logs',
        help='Directory to search for latest model.')
    parser.add_argument('--model', type=str, metavar='PATH',
        help='''\
        Path to saved model. If unspecified automatically select latest one
        found in DIR\
        ''')
    parser.add_argument('-i', '--input', type=str, nargs='+', metavar='DIR',
        default=['data/train/RED/imgset0%d' % i for i in range(300,344,4)] + \
                ['data/train/NIR/imgset0%d' % i for i in range(800,844,4)],
        help='''\
            Input imageset directories. Should contain both LR and HR images.\
            ''')
    parser.add_argument('--no-fuse', action='store_true',
        help='Do not fuse LR images, just use one of them randomly.')
    parser.add_argument('--resize', type=int, metavar='N',
        help='Resize the input images before feeding into model.')
    parser.add_argument('--to-png', action='store_true', help='Store to png.')
    parser.add_argument('--grid', action='store_true')
    parser.add_argument('-o', '--output', type=str, help='Output folder.',
        default='images', metavar='DIR')
    args = parser.parse_args()

    path = args.model
    if path is None:
        latest = max(os.listdir(args.dir))
        path = os.path.join(args.dir, latest, 'export', 'probaV')
        latest = max(os.listdir(path))
        path = os.path.join(path, latest)
        print(path)

    predict_fn = predictor.from_saved_model(path)
    n = 384

    for k,i in enumerate(args.input):
        lrs, hr = load_imageset(i)

        if args.no_fuse:
            lrs = np.expand_dims(lrs[np.random.randint(0,len(lrs))],0)

        if args.resize:
            n = args.resize
            lrs = np.array([resize(i, [n,n], preserve_range=True) for i in lrs])
            n *= 3
            hr = resize(hr, [n,n], preserve_range=True)
            
        pred = predict_fn({'inputs': np.expand_dims(lrs, -1)})['scores']
        pred = np.mean(pred, axis=0)

        if args.grid:
            raise NotImplementedError("this feature coming soon")
        else:
            interp = resize(np.mean(lrs, axis=0), [n,n], preserve_range=True)
            fig = plot_imgs(pred.squeeze(), interp.squeeze(), hr)

            if args.to_png:
                k or os.makedirs(args.output, exist_ok=True)
                path = os.path.join(args.output, os.path.basename(i))
                fig.savefig('%s.png' % path)
            else:
                plt.show()
                
            plt.close(fig)
    
if __name__ == '__main__':
    main()
