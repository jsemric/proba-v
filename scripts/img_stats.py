#!/usr/bin/env python

import os
import numpy as np
from glob import glob

def get_fnames(path):
    return glob(os.path.join(path,'*/*/LR*.png'))

def img_stats(path):
    fnames = get_fnames(path)
    print('# of LR images', len(fnames))
    print('# of LR images including rotation', len(fnames) * 8)

    imgsets = set(map(lambda x: ''.join(x.split('/')[:-1]), fnames))
    print('# of image sets', len(imgsets))
    print('# of image sets including rotation', len(imgsets) * 8)

    print('Estimated memory usage for dataset in MB:',
        (len(fnames)*128*128 + len(imgsets) * (384*384*(1+1/8))) / 1024**2)

def main():
    print('Train:')
    img_stats('data/train')
    print('Test:')
    img_stats('data/test')

if __name__ == '__main__':
    main()