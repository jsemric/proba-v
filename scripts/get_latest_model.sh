#!/bin/bash

get_latest() {
    # cannot copy directories - there are no directories just objects
    latest="`gsutil ls gs://reaver100/ | tail -n 1`"
    echo Letest model: $latest
    MODEL="logs/`basename $latest`"

    if [[ ! -d "$MODEL" ]];then
        echo "Downloading the latest model"
        mkdir -p "$MODEL"
        gsutil -m cp -r "$latest*" "$MODEL"
    else
        echo "Model already in local filesystem"
        exit 1
    fi
}

get_latest