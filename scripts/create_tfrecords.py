#!/usr/bin/env python

import os
import numpy as np
import tensorflow as tf
import argparse
import tempfile
from tqdm import tqdm # fancy progress bar
from glob import glob
from PIL import Image

HR_RESIZE_FACTOR = 3

def _to_bytes_list(data):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[data]))

def convert_image(lr_img_path, lr_mask_path, hr_img_path, hr_mask_path,
    resize_lr=None):

    if resize_lr:

        def g(path, fname, size=resize_lr):
            with open(path,'rb') as f, open(fname, 'wb') as tempf:
                Image.open(path).resize(size).save(tempf)
            return fname

        resize_hr = [i * HR_RESIZE_FACTOR for i in resize_lr]

        lr_img_path = g(lr_img_path, '/tmp/lr.png')
        lr_mask_path = g(lr_mask_path, '/tmp/lr_m.png')
        hr_img_path = g(hr_img_path, '/tmp/hr.png', resize_hr)
        hr_mask_path = g(hr_mask_path, '/tmp/hr_m.png', resize_hr)

    with tf.gfile.FastGFile(lr_img_path, 'rb') as lr_img_file, \
        tf.gfile.FastGFile(lr_mask_path, 'rb') as lr_mask_file, \
        tf.gfile.FastGFile(hr_img_path, 'rb') as hr_img_file, \
        tf.gfile.FastGFile(hr_mask_path, 'rb') as hr_mask_file:

        lr_img = lr_img_file.read()
        lr_mask = lr_mask_file.read()
        hr_img = hr_img_file.read()
        hr_mask = hr_mask_file.read()

    example = tf.train.Example(features=tf.train.Features(feature={
        'lr_image': _to_bytes_list(lr_img),
        'lr_mask': _to_bytes_list(lr_mask),
        'hr_image': _to_bytes_list(hr_img),
        'hr_mask': _to_bytes_list(hr_mask),
    }))

    return example

def convert_images(paths, savepath, resize=None):
    with tf.python_io.TFRecordWriter(savepath) as writer:
        for path in tqdm(paths, savepath):
            hr_img_path = os.path.join(path, 'HR.png')
            hr_mask_path = os.path.join(path, 'SM.png')
            for lr_img_path in glob(os.path.join(path, 'LR*.png')):
                lr_mask_path = lr_img_path.replace('LR','QM')
                example = convert_image(lr_img_path, lr_mask_path, hr_img_path,
                    hr_mask_path, resize_lr=resize)
                writer.write(example.SerializeToString())

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--resize', type=int)
    parser.add_argument('--prefix', type=str)
    args = parser.parse_args()

    resize = None
    if args.resize:
        resize = [args.resize] * 2

    prefix = ''
    if args.prefix:
        prefix = args.prefix + '-'

    path = os.path.join('data','train')
    imgsets = glob(os.path.join(path, 'NIR', '*')) + \
        glob(os.path.join(path, 'RED', '*'))
    np.random.seed(4)
    np.random.shuffle(imgsets)
    n = int(len(imgsets) * .13) # 150 valid, 1010 train
    valid, train = imgsets[:n], imgsets[n:]
    print('Valid: %4d Train: %4d Total: %4d' % \
        (len(valid), len(train), len(imgsets)))

    print('Creating validation TFRecord ...')
    convert_images(valid, 'data/%svalid.tfrecord' % prefix, resize=resize)

    print('Creating training TFRecord ...')
    convert_images(train, 'data/%strain.tfrecord' % prefix, resize=resize)

if __name__ == '__main__':
    main()