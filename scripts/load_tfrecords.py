#!/usr/bin/env python

import tensorflow as tf
import argparse
import numpy as np
import matplotlib.pyplot as plt
import sys
from functools import partial
sys.path.append("..")
from trainer.preprocess import tfr_parser, apply_masks

def show_shapes_and_values(next_element):
    with tf.Session() as sess:
        lr_img, hr_img, lr_mask, hr_mask = sess.run(next_element)
        print(lr_img.shape)
        print(lr_mask.shape)
        print(lr_img[0,  :10])
        print(hr_img[0])

def show_imgs(next_element):
    with tf.Session() as sess:
        fig,axes = plt.subplots(2,3)
        for i in axes.flatten(): i.axis('off')

        imgs = sess.run(next_element)
        for i,e in enumerate(imgs):
            axes[i % 2][i // 2].imshow(e.astype(float).squeeze(), cmap='gray')

        shape = imgs[1].squeeze().shape
        lr, hr = apply_masks(imgs[0], imgs[1], imgs[3], out_shape=shape)
        axes[0][2].imshow(lr.eval().squeeze(), cmap='gray')
        axes[1][2].imshow(hr.squeeze(), cmap='gray')
        plt.show()

def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--input',type=str,default='data/valid.tfrecord')
    args = argparser.parse_args()

    dataset = (
        tf.data.TFRecordDataset(args.input)
        .map(partial(tfr_parser, decode_lr_masks=True))
        .shuffle(256)
    )

    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    # show_shapes_and_values(next_element)
    show_imgs(next_element)
    
if __name__ == '__main__':
    main()