#!/bin/bash

./predict.py \
    --to-png \
    --model output/probav_20190122_194339/export/probaV/1548186229/ \
    --resize 16 \
    --input 'data/train/RED/imgset0348' 'data/train/RED/imgset0350'

xdg-open images/imgset0348.png
