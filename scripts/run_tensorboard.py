#!/usr/bin/env python

import os
import subprocess

latest_model = max(os.listdir('./logs'))
logs = os.path.join('logs', latest_model, 'tboard')
subprocess.call(f'tensorboard --logdir={logs}'.split())