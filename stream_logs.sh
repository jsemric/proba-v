#!/bin/bash
JOB_ID="`gcloud ml-engine jobs list| sed '2q;d' | awk '{print $1}'`"
echo "streaming ${JOB_ID}"
gcloud ml-engine jobs stream-logs $JOB_ID