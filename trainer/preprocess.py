import tensorflow as tf

def distort_image(lr_img, hr_img):
    x = tf.random_uniform([1], 0, 8, dtype=tf.int32)[0]
    lr_img = image_distortions(lr_img, x)
    hr_img = image_distortions(hr_img, x)

    return lr_img, hr_img

def image_distortions(image, x):

    def _in_range(x,a,b):
        return tf.math.logical_and(tf.less_equal(x,b), tf.greater_equal(x,a))

    return tf.case({
        tf.equal(x,0): lambda: tf.transpose(image),
        _in_range(x,1,2): lambda: tf.reverse(image,[x-1]),
        tf.equal(x,3): lambda: tf.reverse(image,[0,1]),
        _in_range(x,4,5): lambda: tf.reverse(tf.transpose(image),[x-4]),
        tf.equal(x,6): lambda: tf.reverse(tf.transpose(image),[0,1])
    }, default=lambda: image, exclusive=True)

def tfr_parser(record, decode_lr_masks=False):
    features = {
        'lr_image': tf.FixedLenFeature([], tf.string),
        'lr_mask': tf.FixedLenFeature([], tf.string),
        'hr_image': tf.FixedLenFeature([], tf.string),
        'hr_mask': tf.FixedLenFeature([], tf.string),
    }
    parsed = tf.parse_single_example(record, features)

    lr_img = tf.image.decode_image(parsed['lr_image'], dtype=tf.uint16)
    lr_img = tf.cast(lr_img, tf.float32) / 65535.

    hr_img = tf.image.decode_image(parsed['hr_image'], dtype=tf.uint16)
    hr_img = tf.cast(hr_img, tf.float32) / 65535.
    hr_mask = tf.image.decode_image(parsed['hr_mask'])
    hr_mask = tf.cast(hr_mask, tf.float32) / 255.

    if decode_lr_masks:
        lr_mask = tf.image.decode_image(parsed['lr_mask'])
        lr_mask = tf.cast(lr_mask, tf.float32) / 255.
        return lr_img, hr_img, lr_mask, hr_mask

    return lr_img, hr_img, hr_mask

def apply_masks(lr_img, hr_img, hr_mask, lr_mask=None):
    hr_img = hr_img * hr_mask

    # can't resize a single image, what a shame!
    lr_shape = tf.shape(lr_img)[:2]
    mask = tf.squeeze(
        tf.image.resize_bicubic(tf.expand_dims(hr_mask,0), lr_shape), 0)
    lr_img = lr_img * mask

    return lr_img, hr_img