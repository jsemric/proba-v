import tensorflow as tf
from functools import partial
from tensorflow.python.estimator.model_fn import ModeKeys as Modes

from trainer.preprocess import apply_masks, tfr_parser, distort_image
from trainer.layers import simple_sr, simple_resnet, two_three_upsampling_resnet

LRX = 128
HRX = 384

def input_fn(filename, count=None, batch_size=16, distort=False,
    dist_strategy=False):

    dataset = (
        tf.data.TFRecordDataset(filename)
        .map(tfr_parser)
        .map(apply_masks)
        # .map(lambda lr,hr: (2.* lr - 1., hr)) # pixel ranges: [-1,1], [0,1]
        .shuffle(256)
        .repeat(count)
        .batch(batch_size)
    )

    if distort:
        dataset = dataset.map(distort_image)

    if dist_strategy:
        # iterator created under the hood, should return (features, labels)
        return dataset
    else:
        iterator = dataset.make_one_shot_iterator()
        lr_img, hr_img = iterator.get_next()
        return lr_img, hr_img

def model_fn(mode, features, labels, params, input_shape=None,
    architecture='resnet', n_blocks=3):

    lr_img = features['inputs'] if isinstance(features, dict) else features
    hr_img = labels

    input_shape = input_shape or (LRX,LRX)
    lr_img = tf.reshape(lr_img, (-1,*input_shape,1))

    is_training = Modes.TRAIN == mode

    if architecture == 'resnet':
        outputs = simple_resnet(lr_img, n_blocks, is_training)
    elif architecture == 'simple':
        outputs = simple_sr(lr_img)
    elif architecture == 'two_three_up':
        outputs = two_three_upsampling_resnet(lr_img, n_blocks, is_training)
    else:
        raise RuntimeError('Invalid architecture name')

    if mode == Modes.PREDICT:
        # Convert predicted_indices back into strings.
        predictions = {
          'scores': outputs
        }
        export_outputs = {
          'prediction': tf.estimator.export.PredictOutput(predictions)
        }
        return tf.estimator.EstimatorSpec(
            mode, predictions=predictions, export_outputs=export_outputs)

    loss = tf.reduce_mean(tf.square(outputs - hr_img), name='mse')
    tf.summary.scalar('loss', loss)

    if mode == Modes.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.0005)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        global_step = tf.train.get_global_step()
        with tf.control_dependencies(update_ops):
            train_op = optimizer.minimize(loss, global_step=global_step)
        return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

    if mode == Modes.EVAL:
        psnr = psnr_metric(outputs, hr_img)
        eval_metric_ops = {
            'psnr': psnr,
        }
        return tf.estimator.EstimatorSpec(
            mode, loss=loss, eval_metric_ops=eval_metric_ops)

def psnr_metric(labels, predictions, weights=None,
              metrics_collections=None,
              updates_collections=None,
              name=None):

    psnr = tf.image.psnr(labels, predictions, 1.0)
    psnr, update_op = tf.metrics.mean(psnr)

    if metrics_collections:
        ops.add_to_collections(metrics_collections, psnr)

    if updates_collections:
        ops.add_to_collections(updates_collections, update_op)

    return psnr, update_op

def serving_input_fn(shape=None):
    if shape is None:
        shape = [LRX,LRX]
    x = tf.placeholder(dtype=tf.float32, shape=[None,*shape,1])
    inputs = {'inputs': x}
    return tf.estimator.export.ServingInputReceiver(inputs, inputs)