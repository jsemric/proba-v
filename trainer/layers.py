import tensorflow as tf
from functools import wraps

def _upscaling_block(inp, up_factor=3):
    # FIXME subpixel layer
    x = tf.layers.conv2d(inp, 64, [3,3], padding='same')
    x = tf.layers.conv2d_transpose(x, 64, [3,3], strides=[up_factor,up_factor],
        padding='same', activation=tf.nn.relu)
    return x

def _residual_block(inp, is_training):
    x = tf.layers.conv2d(inp, 64, kernel_size=[3,3], padding='same')
    x = tf.layers.batch_normalization(x, training=is_training)
    x = tf.nn.relu(x)
    x = tf.layers.conv2d(x, 64, kernel_size=[3,3], padding='same')
    x = tf.layers.batch_normalization(x, training=is_training)
    x = tf.nn.relu(x)
    return tf.add(inp, x)

def resent_wrapper(func):
    """
        Strategy pattern for custom upsampling in resnets architectures.
        Number of conv layers : 1 + N*2 + 1 + M + 1 -> 3 + 2*N + M
        For simple upsampling and default arguments it makes 10 layers
    """
    @wraps(func)
    def wrapper(inp, n_resblocks=3, is_training=False, *args, **kwargs):
        x = tf.layers.conv2d(inp, 64, [9,9], activation=tf.nn.relu,
            padding='same')

        for i in range(n_resblocks):
            x = _residual_block(x, is_training)

        x = tf.layers.conv2d(x, 64, [3,3], padding='same')
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.add(inp, x)

        # custom layers/upscaling
        x = func(x, inp, is_training, *args, **kwargs)

        x = tf.layers.conv2d(x, 1, [3,3], padding='same')
        return x
    return wrapper

@resent_wrapper
def simple_resnet(x, inp=None, is_training=False):
    return _upscaling_block(x)

@resent_wrapper
def two_three_upsampling_resnet(x, inp=None, is_training=False):
    up2 = tf.layers.conv2d(inp, 64, [3,3], activation=tf.nn.relu, 
        padding='same', strides=[2,2])
    up2 = _upscaling_block(up2,3)
    up2 = _upscaling_block(up2,2)
    up3 = _upscaling_block(x)
    return tf.add(up2,up3)

def simple_sr(inp):
    x = tf.layers.conv2d(inp, 64, [9,9], activation=tf.nn.relu, padding='same')
    x = tf.layers.conv2d(inp, 64, [3,3], activation=tf.nn.relu, padding='same')
    x = tf.add(inp, x)
    x = _upscaling_block(x)
    x = tf.layers.conv2d(x, 1, [3,3], padding='same')
    return x

def old_l2_layers(inp, l2_scale=.0003):
    """
        Warning: Deprecated
    """
    conv_layer = partial(tf.layers.conv2d, kernel_size=[3,3], padding='same',
        activation=tf.nn.relu,
        kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=l2_scale)
    )

    x = conv_layer(inp, 64, kernel_size=[9,9],
        kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=l2_scale/3)
    )
    x = conv_layer(x, 64)
    x = conv_layer(x, 64)
    x = conv_layer(x, 64)
    x = conv_layer(x, 1, activation=None)

    return tf.add(x,inp)
