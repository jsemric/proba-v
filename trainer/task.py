import argparse
import os
import tensorflow as tf
from functools import partial

import trainer.model as model

def train_and_evaluate(args):
    input_shape = [16,16] if args.resize else None

    train_input = lambda: model.input_fn(
        args.train_files,
        count=args.num_epochs,
        batch_size=args.train_batch_size,
        distort=not args.no_distort,
        dist_strategy=args.ngpus is not None
    )

    eval_input = lambda: model.input_fn(
        args.eval_files,
        batch_size=args.eval_batch_size,
        count=1,
        distort=False, # never distort eval set
        dist_strategy=args.ngpus is not None
    )

    train_spec = tf.estimator.TrainSpec(train_input, max_steps=args.train_steps)
    exporter = tf.estimator.FinalExporter(
        'probaV',
        partial(model.serving_input_fn, shape=input_shape))

    eval_spec = tf.estimator.EvalSpec(
        eval_input,
        steps=args.eval_steps,
        exporters=[exporter],
        name='probaV-eval')

    strategy = None
    if args.ngpus:
        # distribute to more gpus
        # WARNING cannot use regularizers for some unknown reason
        strategy = tf.contrib.distribute.MirroredStrategy(num_gpus=args.ngpus)

    config = tf.estimator.RunConfig(
        save_checkpoints_steps=args.checkpoint_steps,
        train_distribute=strategy
    )

    estimator = tf.estimator.Estimator(
        model_fn=partial(model.model_fn,
            input_shape=input_shape,
            architecture=args.arch,
            n_blocks=args.n_blocks
        ),
        model_dir=args.job_dir,
        config=config
    )

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--train-files',
        help='GCS file or local paths to training data',
        nargs='+')
    parser.add_argument(
        '--eval-files',
        help='GCS file or local paths to evaluation data',
        nargs='+')
    parser.add_argument(
        '--job-dir',
        help='GCS location to write checkpoints and export models',
        default='/tmp/monkeys')
    parser.add_argument(
        '--num-epochs',
        help="""\
        Maximum number of training data epochs on which to train.
        If both --max-steps and --num-epochs are specified,
        the training job will run for --max-steps or --num-epochs,
        whichever occurs first. If unspecified will run for --max-steps.\
        """,
        type=int,
    )
    parser.add_argument(
        '--train-batch-size',
        help='Batch size for training steps',
        type=int,
        default=16)
    parser.add_argument(
        '--eval-batch-size',
        help='Batch size for evaluation steps',
        type=int,
        default=16)
    parser.add_argument(
        '--train-steps',
        help="""\
        Steps to run the training job for. If --num-epochs is not specified,
        this must be. Otherwise the training job will run indefinitely.\
        """,
        type=int)
    parser.add_argument(
        '--eval-steps',
        help="""\
        Number of steps to run evalution for at each checkpoint.
        If unspecified will run until the input from --eval-files is
        exhausted""",
        default=None,
        type=int)
    parser.add_argument(
        '--resize',
        help='Indicate that datasets are resized versions of the original ones',
        action='store_true')
    parser.add_argument(
        '--ngpus',
        help='Specify the number of GPUs and use distributed strategy.',
        type=int)
    parser.add_argument(
        '--no-distort',
        action='store_true',
        help='Do not distort images.'
    )
    parser.add_argument(
        '--arch',
        help='Choose NN architecture',
        choices=['resnet','simple','two_three_up'],
        default='resnet'
    )
    parser.add_argument(
        '--n-blocks',
        help='''\
        Number of residual blocks in NNs. Default 3. Has no effect if --arch
        simple is set.\
        ''',
        type=int,
        default=3
    )
    parser.add_argument(
        '--checkpoint-steps',
        type=int, default=100)
    parser.add_argument(
        '--verbosity',
        choices=['DEBUG', 'ERROR', 'FATAL', 'INFO', 'WARN'],
        default='INFO',
        help='Set logging verbosity')

    args, _ = parser.parse_known_args()

    # Set python level verbosity.
    tf.logging.set_verbosity(args.verbosity)
    # Set C++ Graph Execution level verbosity.
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = str(
        tf.logging.__dict__[args.verbosity] / 10)
    print(args.train_files, args.eval_files)
    
    train_and_evaluate(args)

if __name__ == '__main__':
    main()