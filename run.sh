#!/bin/bash

JOB_ID="probav_$(date +%Y%m%d_%H%M%S)"

run_local() {
    TRAIN_DATA=$(pwd)/data/small-train.tfrecord
    EVAL_DATA=$(pwd)/data/small-valid.tfrecord
    mkdir -p output

    gcloud ml-engine local train \
        --module-name trainer.task \
        --package-path trainer/ \
        --job-dir output/$JOB_ID \
        -- \
        --train-files $TRAIN_DATA \
        --eval-files $EVAL_DATA \
        --eval-steps 2 \
        --train-steps 2 \
        --train-batch-size 6 \
        --eval-batch-size 6 \
        --resize
}

run_cloud() {
    BUCKET='gs://reaver100'
    TRAIN_DATA=$BUCKET/data/train.tfrecord
    EVAL_DATA=$BUCKET/data/valid.tfrecord
    REGION=europe-west1

    gcloud ml-engine jobs submit training $JOB_ID \
        --runtime-version 1.12 \
        --python-version 3.5 \
        --config ${CONFIG} \
        --staging-bucket ${BUCKET} \
        --module-name trainer.task \
        --package-path trainer/ \
        --job-dir ${BUCKET}/${JOB_ID} \
        --region $REGION \
        -- \
        --train-files $TRAIN_DATA \
        --eval-files $EVAL_DATA \
        --train-batch-size 16 \
        --eval-batch-size 16 \
        --checkpoint-steps 100 \
        --train-steps 1000 \
        --eval-steps 100 \
        --verbosity DEBUG

    if [[ -n $STREAM_LOGS ]];then
        echo "streaming ${JOB_ID}"
        gcloud ml-engine jobs stream-logs $JOB_ID
    fi
}

run_python() {
    TRAIN_DATA=$(pwd)/data/small-train.tfrecord
    EVAL_DATA=$(pwd)/data/small-valid.tfrecord
    mkdir -p output

    python trainer/task.py \
        --train-files $TRAIN_DATA \
        --eval-files $EVAL_DATA \
        --eval-steps 2 \
        --train-steps 2 \
        --train-batch-size 6 \
        --eval-batch-size 6 \
        --resize   
}

mode='local'
CONFIG='config/config.yaml'

while getopts "cpf:s" o; do
    case "${o}" in
        c)
            mode='cloud'
            ;;
        p)
            mode='python'
            ;;
        f)
            CONFIG=${OPTARG}
            ;;
        s)
            STREAM_LOGS="ok"
            ;;
        *)
            echo "Usage: $0 [-c]" 1>&2; exit 1;
            ;;
    esac
done

if [[ $mode == "local" ]];then
    echo "running gcloud locally ..."
    run_local
elif [[ $mode == "python" ]];then
    echo "running locally using python ..."
    run_python
else
    echo "running gcloud in cloud ..."
    run_cloud
fi
