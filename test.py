#!/usr/bin/env python

import unittest
import os
import numpy as np
import tensorflow as tf
from functools import wraps, partial

from trainer.preprocess import tfr_parser

LRX = 128
HRX = 384

class TFRecordsTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        path = os.path.join('data','valid.tfrecord')
        dataset = tf.data.TFRecordDataset(path)

        self.dataset = dataset.map(partial(tfr_parser,decode_lr_masks=True))
        self.iterator = self.dataset.make_one_shot_iterator()
        self.next_element = self.iterator.get_next()
        super().__init__(*args, **kwargs)

    @staticmethod
    def check_bitmap(masks):
        return np.equal(masks,1) | np.equal(masks,0)

    @staticmethod
    def in_range(a, low=0., high=1.):
        return np.greater_equal(a,low) & np.less_equal(a,high)

    def get_next_element(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            with tf.Session() as sess:
                e = sess.run(self.next_element)
                return func(self, *e, *args, **kwargs)
        return wrapper

    @get_next_element
    def test_tuple(self, *args):
        self.assertEqual(len(args),4)

    @get_next_element
    def test_shapes(self, lr_img, hr_img, lr_mask, hr_mask):
        lr_shape = (LRX,LRX,1)
        self.assertEqual(lr_img.shape, lr_shape)
        self.assertEqual(lr_mask.shape, lr_shape)

        hr_shape = (HRX,HRX,1)
        self.assertEqual(hr_img.shape, hr_shape)
        self.assertEqual(hr_mask.shape, hr_shape)

    @get_next_element
    def test_masks(self, lr_img, hr_img, lr_mask, hr_mask):
        self.assertTrue(np.alltrue(TFRecordsTest.check_bitmap(hr_mask)))
        self.assertTrue(np.alltrue(TFRecordsTest.check_bitmap(lr_mask)))

    @get_next_element
    def test_value_range(self, lr_img, hr_img, lr_mask, hr_mask):
        self.assertTrue(np.all(TFRecordsTest.in_range(lr_img)))
        self.assertTrue(np.all(TFRecordsTest.in_range(hr_img)))

if __name__ == '__main__':
    unittest.main()
